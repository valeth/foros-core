import socket
import msgpack
import time
import sys

OWNERS = (
    217078934976724992,
    244285476368941058
)

def connect():
    while True:
        try:
            print("Connecting to socket")
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sock.connect("/tmp/kyouko.sock")
            print("Connected to socket")
            return sock
        except (FileNotFoundError, ConnectionRefusedError, OSError):
            print("Failed to connect, retrying in 5 seconds...", file=sys.stderr)
            time.sleep(5)

def process(sock):
    unpacker = msgpack.Unpacker()

    while True:
        buf = sock.recv(1024**2)
        if not buf: break

        unpacker.feed(buf)

        for unpacked in unpacker:
            handle_message(sock, unpacked)

def handle_message(sock, obj):
    print(obj)
    cid = obj[3][0]
    uid = obj[2][0][0]
    msg = obj[4].decode("utf-8")

    print(f"CID: {cid} | UID: {uid} | MSG: {msg}")
    if not uid in OWNERS: return
    if not msg.startswith("<-test"): return

    packed = msgpack.packb({"channel_id": cid, "payload": { "content": "Tomato" }})
    sock.send(packed)

while True:
    sock = connect()
    process(sock)
    sock.close()
