extern crate foros_core as foros;
extern crate simplelog;
extern crate kankyo as dotenv;


use simplelog::{
    Config as LogConfig,
    TermLogger,
    LogLevelFilter
};

use foros::prelude::*;


fn logger() {
    let log_level = LogLevelFilter::Info;
    let config = LogConfig::default();

    if let Err(why) = TermLogger::init(log_level, config) {
        println!("Failed to initialize logger: {}", why);
    }
}

fn main() {
    dotenv::load()
        .expect("Failed to load environment file");

    logger();

    let mut ipc = foros::ipc::UnixSocketServer::new("/tmp/kyouko.sock")
        .expect("Failed to open IPC connection");

    let mut gateway = foros::gateway::Discord::init()
        .expect("Failed to create gateway");

    let mut bus = MessageBus::new();
    bus.register(&mut ipc)
        .expect("Failed to register IPC bus");
    bus.register(&mut gateway)
        .expect("Failed to register gateway bus");

    ipc.connect();
    gateway.start();
}
