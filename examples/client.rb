require "socket"
require "msgpack"

trap("SIGINT") { exit(0) }

OWNERS = [
    217078934976724992,
    244285476368941058
].freeze

@connected = false
@socket = nil

def connect
  puts "Connecting to socket"
  @socket = @connected ? @socket : UNIXSocket.new("/tmp/kyouko.sock")
  @connected = true
  puts "Connected to socket"
  @socket
rescue Errno::ENOENT, Errno::ECONNREFUSED
  warn "Failed to connect, retrying in 5 seconds"
  sleep(5)
  retry
end

def process(socket)
  unpacker = MessagePack::Unpacker.new(socket)

  unpacker.each do |obj|
    handle_message(obj)
  end
end

def handle_message(obj)
  puts obj.inspect
  cid = obj[3].first
  uid = obj[2].first.first
  msg = obj[4]

  puts "CID: #{cid} | UID: #{uid} | MSG: #{msg}"
  return unless OWNERS.include?(uid)
  return unless msg.start_with?("<-test")

  @socket.write({
    channel_id: cid,
    payload: { content: "Tomato" }
  }.to_msgpack)
end

loop do
  socket = connect
  process(socket)
  @connected = false
end
