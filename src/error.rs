use std::{
    error::Error as StdError,
    result::Result as StdResult,
    fmt::{
        self,
        Display,
        Formatter
    },
    io
};


#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    IpcError,
    BusError
}


pub type Result<T> = StdResult<T, Error>;


impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::IoError(ref err) => err.description(),
            Error::IpcError => "IPC error",
            Error::BusError => "Bus error"
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            Error::IoError(ref err) => err.fmt(f),
            _ => Display::fmt(self, f)
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::IoError(err)
    }
}
