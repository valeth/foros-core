use std::{
    sync::Arc,
    collections::HashMap
};
use parking_lot::{
    Mutex,
    RwLock
};
use mpmc::{Bus, BusReader};
use typemap::Key;
use ::error::{Error, Result};


pub type Receiver = BusReader<Vec<u8>>;
type Proxy = Mutex<Bus<Vec<u8>>>;

pub struct MessageBus {
    bus: Arc<RwLock<HashMap<u64, Proxy>>>
}


pub trait Client {
    fn bus_addr(&self) -> u64;

    fn add_bus(&mut self, bus: MessageBus);
}


impl MessageBus {
    pub fn new() -> Self {
        Self { bus: Arc::new(RwLock::new(HashMap::new())) }
    }

    /// Register a new bus channel
    pub fn register<C>(&mut self, client: &mut C) -> Result<()>
        where C: Client
    {
        let mut bus = self.bus.write();
        if bus.contains_key(&client.bus_addr()) {
            Err(Error::BusError)
        } else {
            client.add_bus(self.clone());
            bus.insert(client.bus_addr(), Mutex::new(Bus::new(10)));
            Ok(())
        }
    }

    /// Broadcast a message on a bus
    pub fn broadcast(&self, addr: u64, msg: &[u8]) -> Result<()> {
        let bus = self.bus.read();
        let bus = bus.get(&addr).ok_or(Error::BusError)?;
        let mut incoming = bus.lock();
        incoming.broadcast(msg.to_owned());
        Ok(())
    }

    /// Adds a receiver to a bus which then can be used to receive broadcasted messages
    pub fn receiver(&self, addr: u64) -> Result<Receiver> {
        let bus = self.bus.read();
        let bus = bus.get(&addr).ok_or(Error::BusError)?;
        let mut outgoing = bus.lock();
        Ok(outgoing.add_rx())
    }
}

impl Clone for MessageBus {
    fn clone(&self) -> Self {
        Self {
            bus: self.bus.clone()
        }
    }
}

impl Key for MessageBus {
    type Value = Self;
}
