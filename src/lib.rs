#[macro_use] extern crate log;
extern crate parking_lot;
extern crate bus as mpmc;
extern crate typemap;
extern crate rmp_serde as msgpack;
extern crate serde;
extern crate serde_json as json;

#[cfg(feature = "discord")]
extern crate serenity;


pub mod error;
pub mod bus;
pub mod ipc;
pub mod gateway;


pub mod prelude {
    pub use ipc::{Interface, UnixSocketServer};
    pub use gateway::{Gateway, Discord};
    pub use bus::MessageBus;
}
