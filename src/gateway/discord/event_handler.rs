use serenity::{
    prelude::*,
    model::{
        channel::Message,
        gateway::Ready
    }
};
use bus::MessageBus;
use msgpack;


pub(crate) struct Handler;


impl EventHandler for Handler {
    fn message(&self, ctx: Context, msg: Message) {
        let bus = {
            let data = ctx.data.lock();
            data.get::<MessageBus>().cloned().unwrap()
        };

        match msgpack::to_vec(&msg) {
            Ok(buf) => {
                if let Err(why) = bus.broadcast(0, &buf) {
                    error!("Failed to broadcast to bus 0: {:?}", why);
                }
            }
            Err(e)  => error!("{:?}", e)
        }
    }

    fn ready(&self, _ctx: Context, _rdy: Ready) {
        info!("Ready");
    }
}
