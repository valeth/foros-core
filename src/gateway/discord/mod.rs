#![cfg(feature = "discord")]

use std::{
    env,
    thread
};
use serenity::{
    prelude::*,
    http::send_message
};
use json;
use msgpack;
use error::Result;
use super::Gateway;
use bus::{
    MessageBus,
    Client as BusClient
};


mod event_handler;


pub struct Discord {
    client: Client,
    bus: Option<MessageBus>
}


impl Discord {
    fn token() -> String {
        env::var("DISCORD_TOKEN")
            .expect("No Discord token set")
    }

    fn dispatcher(&self) {
        if let Some(bus) = self.bus.clone() {
            thread::Builder::new()
                .name("FOROS::Gateway::Dispatcher".to_string())
                .spawn(move || {
                    if let Ok(rx) = bus.receiver(1) {
                        for msg in rx {
                            let tmp: json::Value = msgpack::from_slice(&msg).unwrap();
                            let channel_id = tmp["channel_id"].as_u64().unwrap();
                            info!("{:?}", tmp);
                            let _ = send_message(channel_id, &tmp["payload"]);
                        }
                    }
                })
                .unwrap();
        }
    }
}

impl Gateway for Discord {
    fn init() -> Result<Self> {
        let client = Client::new(&Self::token(), event_handler::Handler)
            .expect("Failed to create Discord client");

        Ok(Self { client, bus: None })
    }

    fn start(mut self) {
        self.dispatcher();
        self.client.start_autosharded()
            .expect("Failed to start Discord gateway");
    }
}

impl BusClient for Discord {
    fn bus_addr(&self) -> u64 { 0 }

    fn add_bus(&mut self, bus: MessageBus) {
        self.bus = Some(bus.clone());
        let mut data = self.client.data.lock();
        data.insert::<MessageBus>(bus);
    }
}
