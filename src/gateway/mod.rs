pub mod discord;


use ::error::Result;

#[cfg(feature = "discord")]
pub use self::discord::Discord;


pub trait Gateway: Sized {
    fn init() -> Result<Self>;

    fn start(self);
}
