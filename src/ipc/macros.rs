macro_rules! with_error_handler {
    [ $cmd:expr; $d:pat => $ok:expr ] => {
        match $cmd {
            Ok($d) => { $ok },
            Err(Error::IoError(ref e)) if e.kind() == io::ErrorKind::WouldBlock => {
                thread::sleep(Duration::from_millis(50))
            },
            Err(why) => {
                error!("{:?}", why);
                break
            }
        }
    }
}
