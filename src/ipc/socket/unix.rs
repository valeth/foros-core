use std::{
    path::Path,
    fs,
    os::unix::net::{UnixStream, UnixListener},
    thread,
    io,
    time::Duration
};

use ::error::{Error, Result};
use ::bus::{
    MessageBus,
    Client as BusClient
};
use ::ipc::{
    interface::Interface,
    client::Client
};

pub struct SocketServer {
    listener: UnixListener,
    bus: Option<MessageBus>
}

impl SocketServer {
    pub fn new<P>(path: P) -> Result<Self>
        where P: AsRef<Path>
    {
        Self::clear_path(&path);
        let sock = UnixListener::bind(path)?;
        sock.set_nonblocking(true)?;
        Ok(Self { listener: sock, bus: None })
    }

    fn listen(self) {
        thread::Builder::new()
            .name("FOROS::IPC::UnixListener".to_string())
            .spawn(move || self.listener())
            .expect("Failed to spawn listener");
    }

    fn listener(self) {
        let mut client_id: u64 = 0;
        info!("Starting listener");
        for stream in self.listener.incoming() {
            with_error_handler!(stream.map_err(Error::IoError); s => {
                if let Some(ref bus) = self.bus {
                    match Self::spawn_client(client_id, s, bus.clone()) {
                        Ok(_) => {
                            info!("Client {}: New connection", client_id);
                            client_id += 1;
                        },
                        Err(why) => {
                            error!("Client {} failed to start: {:?}", client_id, why);
                        }
                    }
                }
            });
        }
    }

    fn spawn_client(id: u64, stream: UnixStream, bus: MessageBus) -> Result<()> {
        stream.set_nonblocking(true)?;
        let client = Client::new(id, stream, bus);
        thread::Builder::new()
            .name(format!("FOROS::Client[{}]", id))
            .spawn(move || client.start())
            .map_err(Error::IoError)?;
        Ok(())
    }

    fn clear_path<P>(path: P)
        where P: AsRef<Path>
    {
        if path.as_ref().exists() {
            fs::remove_file(path).unwrap();
        }
    }
}

impl Interface for SocketServer {
    fn connect(self) {
        self.listen();
    }
}

impl BusClient for SocketServer {
    fn bus_addr(&self) -> u64 { 1 }

    fn add_bus(&mut self, bus: MessageBus) {
        self.bus = Some(bus);
    }
}
