use std::{
    io::{self, Read, Write},
    thread,
    time::Duration
};
use error::{Error, Result};
use bus::{MessageBus, Receiver};


pub struct Client<S>
    where S: Read + Write + Sync + Send
{
    id: u64,
    stream: S,
    bus: MessageBus
}


impl<S> Client<S>
    where S: Read + Write + Sync + Send
{
    pub fn new(id: u64, stream: S, bus: MessageBus) -> Self {
        Self { id, stream, bus }
    }

    pub fn start(self) {
        self.handler();
    }

    fn handler(mut self) {
        if let Ok(mut rx) = self.bus.receiver(0) {
            loop {
                with_error_handler!(self.write_handler(&mut rx); _ => ());
                with_error_handler!(self.read_handler(); _ => ());
            }
        }

        info!("Client {}: Connection closed", self.id);
    }

    fn write_handler(&mut self, rx: &mut Receiver) -> Result<()> {
        match rx.try_recv() {
            Ok(msg) => self.stream.write_all(&msg)?,
            Err(_why) => ()
        }
        Ok(())
    }

    fn read_handler(&mut self) -> Result<()> {
        let mut buf: Vec<u8> = vec![0; 1024];
        let n = self.stream.read(buf.as_mut_slice())?;
        if n == 0 { return Err(Error::IpcError) }
        buf.resize(n, 0);
        self.bus.broadcast(1, &buf)?;
        Ok(())
    }
}
